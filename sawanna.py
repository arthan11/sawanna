# -*- coding: utf-8 -*-
from random import randint, choice, shuffle

RODZAJE_ZWIERZAT = (
    'ANTYLOPA',
    'ŻYRAFA',
    'ZEBRA',
    'SŁOŃ',
    'LEW',
)

KOLORY = (
    'CZERWONY',
    'FIOLETOWY',
    'CZARNY',
    'NIEBIESKI',
    'ZIELONY',
)

DLUGOSC_TORU = 7
ILOSC_TOROW = 4

class Kostka(object):
    def __init__(self):
        self.rzuc()

    def rzuc(self):
        self.value = randint(1, 6)

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return self.__str__()

class Plansza(object):
    def __init__(self, dlugosc_toru, ilosc_torow):
        self.pola = [[None for tor in range(ilosc_torow)] for pole in range(dlugosc_toru)]

    def __str__(self):
        return 'Plansza({}x{})'.format(len(self.pola), len(self.pola[0]))

class Gracz(object):
    def __init__(self, kolor):
        self.kolor = kolor

    def __str__(self):
        return 'Gracz({})'.format(self.kolor)

    def __repr__(self):
        return self.__str__()

class Sawanna(object):
    def __init__(self, ile_graczy=2):
        self.plansza = Plansza(ILOSC_TOROW, DLUGOSC_TORU)
        self.kostki = [Kostka() for i in range(6)]

        self.kolory = list(KOLORY)
        shuffle(self.kolory)
        self.gracze = [Gracz(self.kolory.pop()) for i in range(ile_graczy)]

if __name__ == '__main__':
    sawanna = Sawanna()
    print sawanna.plansza
    print sawanna.gracze
    print sawanna.kostki